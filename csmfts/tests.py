from collections import namedtuple
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import json
from csmrequests.testutils import *

from django.test import TestCase


# class SimpleTest(TestCase):
    # def test_basic_addition(self):
        # """
        # Tests that 1 + 1 always equals 2.
        # """
        # self.assertEqual(1 + 1, 2)
        
       
class CsmTestBrowser(LiveServerTestCase):
    fixtures = ['orders.json','csmxref.json','phone.json','person.json','encounter.json','csmcat.json','queuedata.json','codeval.json','csmreq.json']
    url = ""
   
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)
        CsmTestBrowser.url = self.live_server_url
        CsmTestBrowser.url = CsmTestBrowser.url.replace('localhost','127.0.0.1')

    def tearDown(self):
        self.browser.quit()
        
    def testOpen(self):    
        print '*************************************************************'
        print ' Test - testOpen '
        print '*************************************************************\n'
        testHeader('Valid Request','valid Json')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/open/966175/')
        body = self.browser.find_element_by_tag_name('body')
        compareTo = [{"Status": "IN-PROCESS", "ReqID": 45200512, "OrgID": 966175, "ReqComment": " ", "CreatedBy": "Haven, Scott", "ReqType": "NMSSRTEST                               ", "OrgName": "GB Organization", "QueueData": [{"QueueID": 44480367, "QueueName": "NMSSRqueue                              "}], "CreateDate": "2011-07-20 09:43:09", "SendingSys": "HNAM"}]   
        jret = json.loads(body.text)
        checkData(self, jret, compareTo)
        testFooter('Valid Request', body.text)
     
    #def testOpenInvalid(self):
        testHeader('Invalid request', '[].')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/open/966177/')
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('[]', body.text)
        testFooter('Invalid Request',body.text)
             
    #def testOpenExtra(self):
        testHeader('Empty request', 'error.')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/open/')
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('A server error occurred', body.text)
        testFooter('Empty Request', body.text)
        print '****************************** End - testOpen *******************'
        
    def testViewer(self):        
        print '***************************************************************'
        print '* Test - testViewer '
        print '***************************************************************\n'
        testHeader('Valid Request','valid Json')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/viewer/45200512/')
        body = self.browser.find_element_by_tag_name('body')
        compareTo = {"StatusCD": "IN-PROCESS", "QueueData": [{"QueueID": 44480367, "QueueName": "NMSSRqueue                              "}], "OrgName": "GB Organization", "CreateDate": "2011-07-20 09:43:09", "ReqType": "NMSSRTEST                               ", "PatDOB": "1988-02-04", "AccNbr": "000002011201000012", "MRN": "201674", "OrgID": 966175, "PatName": "Koraddi, Medicaltest", "CreateUser": "Haven, Scott", "PhysicianName": "Vankadaru, Kiran", "PatSex": "Male", "DueDateTm": "2011-07-20 09:45:09", "SendingSys": "HNAM", "ProbComment": " "}
        jret = json.loads(body.text)
        checkData(self,jret,compareTo)
        testFooter('Valid Request', body.text)
        
        #test invalid request
        testHeader('Invalid request', '[].')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/viewer/49999999/')
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('[]', body.text)
        testFooter('Invalid Request',body.text)
        
        #test blank request ID
        testHeader('Empty request', 'error.')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/viewer/')
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('A server error occurred', body.text)
        testFooter('Empty Request',body.text)
        print '************************** End - testViewer **********************\n'
        
    def testPostback(self):
        print '******************************************************************'
        print '* Test - testPostback '
        print '******************************************************************\n'
        testHeader('Valid Postback','valid Json')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/postback/46092687/3123924/8613205/WAIT/2805346/3673040/')
        body = self.browser.find_element_by_tag_name('body')
        compareTo = {"PostStat": 1, "ContactID": "3123924", "RequestID": 46092687, "ErrorText": "ACK"}
        jret = json.loads(body.text)
        checkData(self,jret,compareTo)
        testFooter('Valid Postback',body.text)
        
        testHeader('Invalid Postback','[]')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/postback/44499999/3123924/8613205/WAIT/2805346/3673040/')
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('[]', body.text)
        testFooter('Invalid Postback',body.text)
        print '************************** End - testPostback ********************\n'
        
    def testPhoneStats(self):
        #test phone stats
        print '******************************************************************'
        print '* Test - testPhoneStats '
        print '******************************************************************\n'
        testHeader('Valid Request','valid Json')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/availablephonestatuses/')
        body = self.browser.find_element_by_tag_name('body')
        compareTo = [{"PhoneStatID": 8613203, "PhoneStat": "Busy                                    "}, {"PhoneStatID": 8613204, "PhoneStat": "Disconnected                            "}, {"PhoneStatID": 8613205, "PhoneStat": "No Answer                               "}, {"PhoneStatID": 8613206, "PhoneStat": "Refused to take message                 "}, {"PhoneStatID": 8613207, "PhoneStat": "Wrong Number                            "}, {"PhoneStatID": 43368486, "PhoneStat": "Out of Range                            "}, {"PhoneStatID": 48862427, "PhoneStat": "Testing Description                     "}, {"PhoneStatID": 48864375, "PhoneStat": "KV Ph Status                            "}, {"PhoneStatID": 48864456, "PhoneStat": "BU test BIG seq                         "}, {"PhoneStatID": 2170358000, "PhoneStat": "LONG Ph status                          "}]
        jret = json.loads(body.text)
        checkData(self,jret,compareTo)
        testFooter('Valid Request',body.text)
        print '************************** End - testPhoneStats ******************\n'
        
    def testRequestStatuses(self):
        #test available request statuses
        print '******************************************************************'
        print '* Test - testRequestStatuses '
        print '******************************************************************\n'
        testHeader('Valid Request', 'valid Json')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/availablerequeststatuses/')
        body = self.browser.find_element_by_tag_name('body')
        compareTo = [{"RequestStatDisp": "Completed", "RequestStatID": 1884}, {"RequestStatDisp": "In-Process", "RequestStatID": 1885}, {"RequestStatDisp": "New", "RequestStatID": 1886}, {"RequestStatDisp": "Wait", "RequestStatID": 1887}]
        jret = json.loads(body.text)
        checkData(self,jret,compareTo)
        testFooter('Valid Request',body.text)
        print '************************** End - testRequestStatuses *************\n'
        
        #test all
    def testAllRequests(self):
        print '******************************************************************'
        print ' Test - testAllRequests '
        print '******************************************************************\n'
        testHeader('Valid Request w/o Org', 'valid Json')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/all/2011-07-01/2011-07-21/')
        body = self.browser.find_element_by_tag_name('body')
        compareTo = [{"Status": "IN-PROCESS", "ReqID": 45200512, "OrgID": 966175, "ReqComment": " ", "CreatedBy": "Haven, Scott", "ReqType": "NMSSRTEST ", "OrgName": "GB Organization", "QueueData": [{"QueueID": 44480367, "QueueName": "NMSSRqueue "}], "CreateDate": "2011-07-20 09:43:09", "SendingSys": "HNAM"}]
        jret = json.loads(body.text)
        checkData(self,jret,compareTo)
        testFooter('Valid Request w/o Org',body.text)
        
        testHeader('Valid Request w Org', 'valid Json')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/all/2011-07-01/2011-07-21/966175/')
        body = self.browser.find_element_by_tag_name('body')
        compareTo = [{"Status": "IN-PROCESS", "ReqID": 45200512, "OrgID": 966175, "ReqComment": " ", "CreatedBy": "Haven, Scott", "ReqType": "NMSSRTEST ", "OrgName": "GB Organization", "QueueData": [{"QueueID": 44480367, "QueueName": "NMSSRqueue "}], "CreateDate": "2011-07-20 09:43:09", "SendingSys": "HNAM"}]
        jret = json.loads(body.text)
        checkData(self,jret,compareTo)
        testFooter('Valid Request w Org',body.text)
        print '************************** End - testAllRequests *****************\n'
        
    def testAccTrack(self):
        print '******************************************************************'
        print ' Test - testAccTrack '
        print '******************************************************************\n'
        testHeader('Valid Request w/o container', 'valid Json')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/acctrack/11-201-00012/')
        body = self.browser.find_element_by_tag_name('body')
        compareTo = [{"ContainerInfo": [{"ContainerEvents": [{"EventDtTm": "2011-07-20 09:41:45", "UserID": 1, "UpdtEvent": "Dispatched", "CurrentLocID": 0, "CurrentLoc": " "}, {"EventDtTm": "2011-07-20 09:42:21", "UserID": 4528188, "UpdtEvent": "Collected", "CurrentLocID": 772010426, "CurrentLoc": "BM Specimen Login"}, {"EventDtTm": "2011-07-20 09:42:21", "UserID": 4528188, "UpdtEvent": "Received", "CurrentLocID": 772010426, "CurrentLoc": "BM Specimen Login"}], "ContainerID": 18197588}], "AccnNbr": "11-201-00012"}]
        jret = json.loads(body.text)
        checkData(self,jret,compareTo)
        testFooter('Valid Request w/o container',body.text)
        
        testHeader('Valid Request w container','valid Json')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/acctrack/11-201-00012a/')
        body = self.browser.find_element_by_tag_name('body')
        compareTo = [{"ContainerInfo": [{"ContainerEvents": [{"EventDtTm": "2011-07-20 09:41:45", "UserID": 1, "UpdtEvent": "Dispatched", "CurrentLocID": 0, "CurrentLoc": " "}, {"EventDtTm": "2011-07-20 09:42:21", "UserID": 4528188, "UpdtEvent": "Collected", "CurrentLocID": 772010426, "CurrentLoc": "BM Specimen Login"}, {"EventDtTm": "2011-07-20 09:42:21", "UserID": 4528188, "UpdtEvent": "Received", "CurrentLocID": 772010426, "CurrentLoc": "BM Specimen Login"}], "ContainerID": 18197588}], "AccnNbr": "11-201-00012A"}]
        jret = json.loads(body.text)
        checkData(self,jret,compareTo)
        testFooter('Valid Request w container',body.text)
        
        testHeader('Invalid Request','[]')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/acctrack/11-201-000099')
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('[]', body.text)
        testFooter('Invalid Request',body.text)
        print '************************** End - testAccTrack ********************\n'
        
    def testOrgPhoneInfo(self):
        print '******************************************************************'
        print ' Test - testOrgPhoneInfo '
        print '******************************************************************\n'
        testHeader('Valid Request','valid Json')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/orgphoneinfo/966175/')
        body = self.browser.find_element_by_tag_name('body')
        compareTo = [{"PhoneNum": "0000000001", "PhoneID": 3673039, "PhoneExt": "1"}, {"PhoneNum": "0000000002", "PhoneID": 3673040, "PhoneExt": "2"}]
        jret = json.loads(body.text)
        checkData(self,jret,compareTo)
        testFooter('Valid Request',body.text)
        
        testHeader('Empty Request','error')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/orgphoneinfo/')
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('A server error occurred', body.text)
        testFooter('Empty Request',body.text)
        print '************************** End - testOrgPhoneInfo ****************\n'
        
    def testOrgContacts(self):
        print '******************************************************************'
        print ' Test - testOrgContacts '
        print '******************************************************************\n'
        testHeader('Valid Request','valid Json')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/orgcontacts/966175/')
        body = self.browser.find_element_by_tag_name('body')
        compareTo = [{"ContactID": 3054417, "ContactName": "Physician ,Physician"}, {"ContactID": 3123924, "ContactName": "Shropshire ,Justin"}, {"ContactID": 3017327, "ContactName": "Tech ,Dba"}, {"ContactID": 2886998, "ContactName": "Premakumar ,Pradeep"}, {"ContactID": 3531931, "ContactName": "Mr.,Hlavaty"}]
        jret = json.loads(body.text)
        checkData(self,jret,compareTo)
        testFooter('Valid Request',body.text)
    
        testHeader('Empty Request','valid Json')
        self.browser.get(CsmTestBrowser.url + '/csmrequests/orgcontacts/')
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('A server error occurred',body.text)
        testFooter('Empty Request',body.text)
        print '************************** End - testOrgContacts *****************\n'


        
        