from csmrequests.models import Accession
#####################################################################################
#
#  FUNCTION: formatDtTm
#  Parameters: DtTm - A DateTimeField value from the database
#
#            Called by: any
#
#            Returns: a string with date and time in it
#                     '2007-02-12 16:47:42'
#
####################################################################################
def formatDtTm(DtTm):
  return DtTm.date().isoformat() + ' ' + DtTm.time().isoformat()

#####################################################################################
#
#  FUNCTION: addCentury
#  Parameters: AccYear - the year from the formatted accession
#
#            Called by: parseFmtAccn
#
#            Returns: 4 digit year string
#
####################################################################################
def addCentury( AccYear ):
  strYear = AccYear
  if len(AccYear) == 2:
    if int(AccYear) > 80:
      strYear = '19' + AccYear
    else:
      strYear = '20' + AccYear
  return strYear


#####################################################################################
#
#  FUNCTION: parseFmtAccn
#  Parameters: FmtAccn - formatted accession
#
#            Called by: any
#
#            Returns: unformatted accession
#                     if unrecognized format passed in, return zeros for accn
#
####################################################################################
def parseFmtAccn( FmtAccn ):

  #If there is a container ID on the accession, truncate it before parsing
  if FmtAccn[-1:].isalpha():
    FmtAccn = FmtAccn[:-1]

  AccnArray = FmtAccn.split('-')

  JULIAN_SIZE = 3

  unfmtAccn = ''

  if len(AccnArray) == 3:   #no site prefix
    if AccnArray[0].strip().isalpha():  #there is an alpha prefix
      AccnArray[1] = addCentury(AccnArray[1])
      AccnArray[2] = AccnArray[2].zfill(7)
    else:
      AccnArray[0] = addCentury(AccnArray[0])
      AccnArray[1] = AccnArray[1].zfill(JULIAN_SIZE)
      AccnArray[2] = AccnArray[2].zfill(6)

    unfmtAccn = '00000' + ''.join(AccnArray)

  elif len(AccnArray) == 4:    #site prefix exists
    AccnArray[0] = AccnArray[0].zfill(5)
    if AccnArray[1].strip().isalpha():  #there is an alpha prefix
      AccnArray[2] = addCentury(AccnArray[2])
      AccnArray[3] = AccnArray[3].zfill(7)
    else:
      AccnArray[1] = addCentury(AccnArray[1])
      AccnArray[2] = AccnArray[1].zfill(JULIAN_SIZE)
      AccnArray[3] = AccnArray[2].zfill(6)
    unfmtAccn = ''.join(AccnArray)
  else:                             #unrecognized format
    unfmtAccn = unfmtAccn.zfill(20)
  return unfmtAccn


#####################################################################################
#
#  FUNCTION: getContanerID
#  Parameters: FmtAccn - formatted accession
#
#            Called by: any
#
#            Returns: integer mapped to alpha char - A=1, B=2, etc...
#                     if no alpha id is found, 0 is returned
#
####################################################################################
def getContainerID( FmtAccn ):
  if FmtAccn[-1:].isalpha():   #check last character for alpha
    contID = FmtAccn[-1:]
    return ord(contID.upper())-64
  else:
    return 0

#####################################################################################
#
#  FUNCTION: getAlphaContanerID
#  Parameters:  integer < 27
#
#            Called by: any
#
#            Returns: alpha character mapped for 1=A, 2=B, etc...    
#                     if 0 is passed in, a blank string will be returned ""
#
####################################################################################
def getAlphaContainerID( ContainerNbr ):
  if str(ContainerNbr).isdigit() and ContainerNbr < 27 and ContainerNbr != 0:
    return chr(ContainerNbr + 64)
  else:
    return ""


#####################################################################################
#
#  FUNCTION: buildFmtAccn
#  Parameters: UnfmtAccn - unformatted accession
#
#            Called by: any
#
#            Returns: formatted accession
#
####################################################################################
def buildFmtAccn( UnfmtAccn ):
  qq = Accession.objects.get(accession=UnfmtAccn)

  fmtAccn = ''
  #check for site prefix
  if qq.site_prefix_cd_id_id > 0:
    sp = qq.site_prefix_cd_id.cdf_meaning.zfill(2)
    fmtAccn = sp + '-'
  else:
    sp = ''

  #check for alpha prefix
  if qq.alpha_prefix > "  ":
    ap = qq.alpha_prefix
    fmtAccn += ap + '-'
    fmtAccn += str(qq.accession_year)[-2:] + '-' + str(qq.accession_sequence_nbr).zfill(5)
  else:
    fmtAccn += str(qq.accession_year)[-2:] + '-' + str(qq.accession_day).zfill(3) + '-' + str(qq.accession_sequence_nbr).zfill(5)

  return fmtAccn
