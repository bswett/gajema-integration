##############################################################
#  import of classes
##############################################################
from django.core import serializers
from django.http import HttpResponse
from csmrequests.utils import *
from csmrequests.models import AccessionOrderR
from csmrequests.models import CodeValue
from csmrequests.models import ContainerAccession
from csmrequests.models import ContainerEvent
from csmrequests.models import CsmPhoneStats
from csmrequests.models import CsmQCatXref
from csmrequests.models import CsmRequests
from csmrequests.models import Orders
from csmrequests.models import PersonAlias
from csmrequests.models import Phone
from csmrequests.models import CsmLstContact
from csmrequests.models import Prsnl
from csmrequests.models import PrsnlOrgReltn
from django.db.models import F
import datetime
import json

##################################################################
#
#  Function: BuildRequests
#  Called by: OpenRequests,
#             AllRequests,
#             AllRequestsOrg
#  Input: CsmRequests filtered queryset
#  Returns: JSON Object
#  Classes: CsmRequests, CsmQCatXref
#
##################################################################

def BuildRequests( qq ):
    data = ""
    list=[]

    for i in qq:
# retrieve values from CsmQCatXref based on category ID and subcategory ID

        rr = CsmQCatXref.objects.filter(csm_cat=i.csm_cat.csm_cat_id,csm_sub_cat_id=i.csm_sub_cat.csm_sub_cat_id)   
        qname = []  # list for storing queue data
        for j in rr:
            try:
                qname.append({'QueueID':j.csm_queue.csm_queue_id,
                              'QueueName':j.csm_queue.csm_queue_desc})
            except:
                pass

# populate list that will be sent as json string
        try:
            list.append({'OrgID':i.organization_id,
                         'OrgName':i.organization.org_name,
                         'ReqID':i.csm_req_id,
                         'CreateDate':formatDtTm(i.csm_ord_dt_tm),
                         'CreatedBy':i.updt.name_full_formatted,
                         'Status':i.status_cd_id.cdf_meaning,
                         'ReqComment':i.csm_req_com.long_text,
                         'ReqType':i.csm_cat.csm_cat_desc,
                         'QueueData':qname,
                         'SendingSys':'HNAM'})
        except:
            pass

    data = json.dumps(list)
    return data

##################################################################
#
#  Function: BuildViewRequest
#  Called by: CsmViewer
#  Input:   CsmRequests filtered queryset
#  Returns: JSON Object
#  Classes: CsmRequests, CsmQCatXref, Phone, PrsnlOrgReltn, Prsnl,
#           CsmPhoneStats, PersonAlias, CodeValue
#
##################################################################

def BuildViewRequest( i ):
    data = ""
    list = []

# retrieve queue info from CsmQCatXref based on category ID and subcategory ID
    rr=CsmQCatXref.objects.filter(csm_cat=i.csm_cat_id,csm_sub_cat_id=i.csm_sub_cat.csm_sub_cat_id)
    qname=[]
    for j in rr:
        try:
            qname.append({'QueueID':j.csm_queue.csm_queue_id,
                          'QueueName':j.csm_queue.csm_queue_desc})
        except:
            pass

# retrieve phone information for the organization related to the request
#    ph=Phone.objects.filter(parent_entity_id=i.organization_id).filter(parent_entity_name='ORGANIZATION').fil#ter(active_ind=1)
#    phonelist=[]
#    for k in ph:
#        phonelist.append({'PhoneID':k.phone_id,
#                          'PhoneNum':k.phone_num,
#                          'PhoneExt':k.extension})

# retrieve all phone statuses
#    cps=CsmPhoneStats.objects.filter(csm_phone_stat_id__gt=0)
#    phonestatlist = []
#    for m in cps:
#        phonestatlist.append({'PhoneStatID':m.csm_phone_stat_id,
#                              'PhoneStat':m.csm_phone_stat_desc})

# retrieve contact information for the organization
#    oo=PrsnlOrgReltn.objects.filter(organization_id=i.organization_id).filter(active_ind=1)
#    contactlist = []
#    for z in oo:
#        try:
#            contactlist.append({'ContactName':z.person.name_full_formatted,
#                                    'ContactID':z.person_id})
#        except:
#            pass

#load patient information
    SexCd = 0
    MRN = ""
    PatName = ""
    PatID = ""
    PhysicianName = ""
    Accession = ""
    PatDOB = ""

    if i.csm_req.parent_entity_name.strip() == 'ORDERS':
        try:
            PatName = i.csm_req.parentObj().person.name_full_formatted
            Accession = i.csm_req.getAccn().accession
            PatID = i.csm_req.parentObj().person.person_id
            SexCd = i.csm_req.parentObj().person.sex_cd
        except:
            pass

        if i.csm_req.parentObj().person.birth_dt_tm is not None:
            PatDOB = i.csm_req.parentObj().person.birth_dt_tm.isoformat()
        else:
            PatDOB = ""

        mr = PersonAlias.objects.filter(person=i.csm_req.parentObj().person.person_id).filter(person_alias_type_cd=10).filter(active_ind=1)

#
# This logic displays the MRN. It was decided to only display the first active MRN
# instead of all MRNs in the the PersonAlias table.
#
#            for m in mr:
#                patalias.append({'MRN':m.alias})

        MRN = '0';
        if mr.count() > 0:
            MRN=mr[0].alias

        if i.csm_req.parentObj().last_update_provider is not None:
            PhysicianName = i.csm_req.parentObj().last_update_provider.name_full_formatted
        else:
            PhysicianName = ""
    elif i.csm_req.parent_entity_name.strip() == 'ENCOUNTER':
        try:
            PatName = i.csm_req.parentObj().person.name_full_formatted
            SexCd = i.csm_req.parentObj().person.sex_cd
            PatID = i.csm_req.parentObj().person.person_id
        except:
            pass

        if i.csm_req.parentObj().person.birth_dt_tm is not None:
            PatDOB = i.csm_req.parentObj().person.birth_dt_tm.isoformat()
        else:
            PatDOB = ""

        mr = PersonAlias.objects.filter(person=i.csm_req.parentObj().person.person_id).filter(person_alias_type_cd=10).filter(active_ind=1)

#
# This logic displays the MRN. It was decided to only display the first active MRN
# instead of all MRNs in the the PersonAlias table.
#
#            for m in mr:
#                patalias.append({'MRN':m.alias})
        MRN = '0';
        if mr.count() > 0:
            MRN=mr[0].alias
    else:
        pass

    if SexCd > 0:
        sx=CodeValue.objects.filter(code_set=57).filter(code_value=SexCd)
        PatSex=sx[0].display
    else:
        PatSex = ""

    list = {'OrgID':i.organization_id,
            'OrgName':i.organization.org_name,
#                 'OrgPhone':phonelist,
#                 'PhoneStats':phonestatlist,
#                 'OrgContacts':contactlist,
            'CreateDate':formatDtTm(i.csm_ord_dt_tm),
            'CreateUser':i.updt.name_full_formatted,
            'MRN':MRN,
            'PatName':PatName,
            'PatSex':PatSex,
            'PatDOB':PatDOB,
            'ReqType':i.csm_cat.csm_cat_desc,
            'ProbComment':i.problem_req_comment.long_text,
            'QueueData':qname,
            'DueDateTm':formatDtTm(i.csm_due_dt_tm),
            'PhysicianName':PhysicianName,
            'AccNbr':Accession,
            'StatusCD':i.status_cd_id.cdf_meaning,
            'SendingSys':'HNAM'}

    ReturnData = json.dumps(list)

    return ReturnData

#####################################################################################
#
#  FUNCTION: BuildPostBack
#
#  Called by: PostBack
#  Input:   qq: CsmRequests filtered queryset
#           ContactID, UserID, PhoneID: parameters from URL
#           Status: cdf_meaning passed in as the desired value to update the request status
#
#  Returns: JSON object (ACK)
#
####################################################################################

def BuildPostBack( qq, ContactID, PhoneStatID, Status, UserID, PhoneID ):

    ReturnData = ""
    AckMSG = "ACK"
    UpdtDtTM = ""
    ErrDesc = ""

    try:
        Contact=Prsnl.objects.get(person=ContactID)
    except:
        Contact=""
        AckMSG="NACK"
        ErrDesc =" Contact ID Invalid"

    try:
        PhCheck=Phone.objects.get(phone_id=PhoneID)
    except:
        PhCheck=""
        AckMSG="NACK"
        ErrDesc =" Phone ID Invalid"

    try:
        PhStat = CsmPhoneStats.objects.get(csm_phone_stat_id = PhoneStatID)
    except:
        PhStat = ""
        AckMSG="NACK"
        ErrDesc =" Phone Status ID Invalid"

    try:
        User=Prsnl.objects.get(person=UserID)
    except:
        User=""
        AckMSG="NACK"
        ErrDesc =" User ID Invalid"

    StatCheck = CodeValue.objects.filter(cdf_meaning=Status).filter(code_set=2213)

    if StatCheck.count() == 0:
        StatCheck = []
        AckMSG="NACK"
        ErrDesc = " Status Invalid"

    if AckMSG == 'NACK':
        PostBackStat = 0
        PostAck=[]
        PostAck = {'ContactID':ContactID,
                   'PostStat':PostBackStat,
                   'ErrorText':AckMSG + ErrDesc}
        ReturnData = json.dumps(PostAck)
        return ReturnData
    else:
        PostBackStat = 1

    #get the code value for the currently stored request status in order to compare to the status passed in for updating 
    CSMStatus = CodeValue.objects.get(code_value=qq.status_cd_id.code_value)

    if CSMStatus.cdf_meaning != Status:
        cv = CodeValue.objects.filter(cdf_meaning=Status).filter(code_set=2213)
        qq.status_cd_id = cv[0]
        if Status == "COMPLETED":
            qq.csm_compl_dt_tm = datetime.datetime.now().replace(microsecond=0)
        qq.updt_id = UserID
        qq.updt_task = 0
        qq.updt_cnt= F('updt_cnt')+1
        qq.updt_applctx = 0
        qq.saveCsm()

    NewRow = False

    try:
        cc = CsmLstContact.objects.get(phone__phone_id = PhoneID)
    except:
        cc = CsmLstContact() #create new instance of CsmLstContact
        cc.phone_id = PhoneID
        cc.csm_lst_dt_tm = datetime.datetime.now().replace(microsecond=0)
        cc.updt_cnt=0
        cc.contact=" "
        NewRow = True

    if cc.csm_phone_stat_id != PhStat.csm_phone_stat_id:
        cc.csm_phone_stat = PhStat
        pers = Prsnl.objects.get(person__person_id = ContactID)
        cc.person = pers
        cc.updt_id = UserID
        cc.updt_task = 0
        if not NewRow:
            cc.updt_cnt=F('updt_cnt')+1
        cc.updt_applctx = 0
        cc.saveLstContact()

    PostAck=[]

    PostAck = {'ContactID':ContactID,
               'RequestID':qq.csm_req_id,
               'PostStat':PostBackStat,
               'ErrorText':AckMSG}

    ReturnData = json.dumps(PostAck)

    return ReturnData

#####################################################################################
#
#  FUNCTION: BuildAccTrack
#
#  Called by: AccessionTracking
#  Input:   qq: ContainerAccession filtered queryset
#           ContID: numeric container id
#
#  Returns: JSON object
#
####################################################################################

def BuildAccTrack( qq, ContID ):

    ReturnData = ""
    AccTrk = []
    Container = []

    for a in qq:

#        rr = ContainerEvent.objects.filter(container_id=a.container_id)
        #using custom manager to retrieve data to allow for Millennium model to avoid having a primary key
        rr = ContainerEvent.objects.getContEventsByID(a.container_id)
        #rr now contains a list of python 'dict' objects as opposed to a queryset. 

        ContEvents = []

        for b in rr:
            CurLoc=CodeValue.objects.get(code_value=b['current_location_cd']) #accessing the 'dict' object requires different notation
            try:
                Event=CodeValue.objects.get(code_value=b['event_type_cd'])
                EvtDisp = Event.display
            except:
                Event=""
                EvtDisp=""

            ContEvents.append({'UpdtEvent':EvtDisp,
                               'UserID':b['updt_id'],
                               'EventDtTm':formatDtTm(b['updt_dt_tm']),
                               'CurrentLocID':b['current_location_cd'],
                               'CurrentLoc':CurLoc.display})

        Container.append({'ContainerID':a.container_id,
                          'ContainerEvents':ContEvents})


    fmtAccn = buildFmtAccn(a.accession) + getAlphaContainerID(ContID)

    AccTrk.append({'AccnNbr':fmtAccn,
                   'ContainerInfo':Container})


    ReturnData = json.dumps(AccTrk)

    return ReturnData

#####################################################################################
#
#  FUNCTION: BuildAccOrdLst
#
#  Called by: AccessionQuery
#  Input:   qq: AccessionOrderR filtered queryset
#           ContID: numeric container ID 
#
#  Returns: JSON object
#
####################################################################################

def BuildAccOrdLst( qq, ContID ):

    ReturnData = ""
    AccLst = []
    AccObj = []

    for i in qq:

        OrdStat=CodeValue.objects.filter(code_set=6004).filter(code_value=i.order.order_status_cd)
        OrderStat=OrdStat[0].display

        if i.order.last_update_provider is not None:
            PhysicianName = i.order.last_update_provider.name_full_formatted
        else:
            PhysicianName = ""


        AccLst.append({'OrderMnem':i.order.order_mnemonic,
                       'OrderStat':OrderStat,
                       'OrderDtTm':i.order.orig_order_dt_tm.isoformat(),
#                       'CurrentLocID':b.current_location_cd,
                       'OrdDoc':PhysicianName})

        fmtAccn = buildFmtAccn(qq[0].accession) + getAlphaContainerID(ContID)

    AccObj.append({'AccNbr':fmtAccn,
                   'OrderList':AccLst})

    ReturnData = json.dumps(AccObj)

    return ReturnData

#####################################################################################
#
#  FUNCTION: BuildRequestStatuses
#
#  Called by: AvailableRequestStatuses
#  Input:   qq: Request Statuses filtered queryset (Code_Value = 2213)
#
#  Returns: JSON object
#
####################################################################################

def BuildRequestStatuses( qq ):
    ReturnData = ""
    ReqStatLst = []

    for i in qq:

        ReqStatLst.append({'RequestStatID':i.code_value,
                       'RequestStatDisp':i.display})

    ReturnData = json.dumps(ReqStatLst)

    return ReturnData

#####################################################################################
#
#  FUNCTION: BuildPhoneStatuses
#
#  Called by: AvailablePhoneStatuses
#  Input:   cps: Phone Statuses filtered queryset
#
#  Returns: JSON object
#
####################################################################################

def BuildPhoneStatuses( cps ):

    ReturnData = ""
    phonestatlist = []

    for m in cps:
        phonestatlist.append({'PhoneStatID':m.csm_phone_stat_id,
                              'PhoneStat':m.csm_phone_stat_desc})

    ReturnData = json.dumps(phonestatlist)

    return ReturnData

#####################################################################################
#
#  FUNCTION: BuildOrgContacts
#
#  Called by: AvailableOrgContacts
#  Input:   oo: PrsnlOrgReltn filtered queryset (active_ind = 1, Organization_id = OrgID_IN)
#
#  Returns: JSON object
#
####################################################################################

def BuildOrgContacts( oo ):
    ReturnData = ""
    ReqOrgContacts = []

    for z in oo:
        try:
            ReqOrgContacts.append({'ContactName':z.person.name_full_formatted,
                                    'ContactID':z.person_id})
        except:
            pass

    ReturnData = json.dumps(ReqOrgContacts)

    return ReturnData

#####################################################################################
#
#  FUNCTION: BuildPhoneInfo
#
#  Called by: OrgPhoneInfo
#
#  Input:   ph: Phone filtered queryset (active_ind = 1, parent_entity_id = OrgID_IN)
#
#  Returns: JSON object
#
####################################################################################

def BuildPhoneInfo( ph ):
    ReturnData = ""

    phonelist=[]
    for k in ph:
        phonelist.append({'PhoneID':k.phone_id,
                          'PhoneNum':k.phone_num,
                          'PhoneExt':k.extension})

    ReturnData = json.dumps(phonelist)

    return ReturnData

#####################################################################################
#
#  FUNCTION: BuildRequestStatuses
#
#  Called by: AvailableRequestStatuses
#  Input:   qq: Values from the code value table (Code_set=2213) 
#
#  Returns: JSON object
#
####################################################################################
def BuildRequestStatuses( qq ):
    ReturnData = ""
    StatList = []

    for i in qq:
        StatList.append({'RequestStatID':i.code_value,
                         'RequestStatDisp':i.display})
        
    ReturnData = json.dumps(StatList)

    return ReturnData


########################################################################################
#
#   SERVICE CALLS - SET IN URLS.PY
#
########################################################################################

########################################################################################
#
#   SERVICE: OpenRequests
#
#   URL: .../open/<P1>
#
#   P1: Organization ID
#
#   DESCRIPTION: Provides method for passing an ORGANIZATION_ID to retrieve the details
#                of all open requests found in csm_requests table for a specified
#                organization
#
#   FUNCTION(S) CALLED: BuildRequests
#
########################################################################################

def OpenRequests(request, Fnc, OrgID_in):
    qq=CsmRequests.objects.filter(organization=OrgID_in).exclude(status_cd_id__cdf_meaning='COMPLETED')

    data = BuildRequests( qq )

    return HttpResponse(data, mimetype='application/json')

########################################################################################
#
#   SERVICE: AllRequests
#
#   URL: .../all/<P1>/<P2>/<P3>
#
#   P1: Start date in the format: YYYY-MM-DD
#   P2: End date in the formate:  YYYY-MM-DD
#   P3: Organization ID           <Optional>
#
#   DESCRIPTION: Provides method for passing an ORGANIZATION_ID to retrieve the details
#                of all requests created within the date range specified by start date
#                and end date and, optionally, (see AllRequestsOrg) when provided, for
#                specified organization found in csm_requests table.
#
#   FUNCTION(S) CALLED: BuildRequests
#
########################################################################################

def AllRequests(request, Fnc,StartDT, EndDT):

    StartYMD = StartDT.split('-')
    EndYMD = EndDT.split('-')

    qq=CsmRequests.objects.filter(csm_ord_dt_tm__gte=datetime.date(int(StartYMD[0]),int(StartYMD[1]),int(StartYMD[2])),csm_ord_dt_tm__lte=datetime.date(int(EndYMD[0]),int(EndYMD[1]),int(EndYMD[2])))
    data = BuildRequests( qq )
    return HttpResponse(data)

########################################################################################
#
#   SERVICE: AllRequestsOrg
#
#   URL: .../all/<P1>/<P2>/<P3>
#
#   P1: Start date in the format: YYYY-MM-DD
#   P2: End date in the formate:  YYYY-MM-DD
#   P3: Organization ID           <Optional>
#
#   DESCRIPTION: Provides method for passing an ORGANIZATION_ID to retrieve the details
#                of all requests created within the date range specified by start date
#                and end date and, optionally, when provided, for specified organization
#                found in csm_requests table.
#
#   FUNCTION(S) CALLED: BuildViewReq
#
#######################################################################################

def AllRequestsOrg(request, Fnc, StartDT, EndDT, OrgID_in):
    StartYMD = StartDT.split('-')
    EndYMD   = EndDT.split('-')

    qq=CsmRequests.objects.filter(organization=OrgID_in).filter(csm_ord_dt_tm__gte=datetime.date(int(StartYMD[0]),int(StartYMD[1]),int(StartYMD[2])),csm_ord_dt_tm__lte=datetime.date(int(EndYMD[0]),int(EndYMD[1]),int(EndYMD[2])))
    data = BuildRequests( qq )

    return HttpResponse(data)

########################################################################################
#
#   SERVICE: CsmViewer
#
#   URL: .../viewer/<P1>
#
#   P1: Request ID to query
#
#   DESCRIPTION: Provides method for passing a REQUEST_ID to retrieve the details of
#                the specified request found in csm_requests table.
#
#   FUNCTION(S) CALLED: BuildViewReq
#
########################################################################################

def CsmViewer(request, Fnc, RequestID_in):
    try:
        qq=CsmRequests.objects.get(csm_req_id=RequestID_in)
    except:
        return HttpResponse ("[]")

    FmtData = BuildViewRequest(qq)

    return HttpResponse(FmtData, mimetype='application/json')

########################################################################################
#
#   SERVICE: AccessionTracking
#
#   URL: .../acctrack/<P1>
#
#   P1: Accession Number
#
#   DESCRIPTION: Provides method for passing from HNAM all tracking events related to
#                the accession specified as an input parameter to the service.
#
#   FUNCTION(S) CALLED: BuildAccTrack
#
########################################################################################

def AccessionTracking(request, Fnc, Accession_in):

    CalledAccn = parseFmtAccn(Accession_in)
    ContID = getContainerID(Accession_in)

    if ContID > 0:
        qq=ContainerAccession.objects.filter(accession=CalledAccn).filter(accession_container_nbr=ContID)
    else:
        qq=ContainerAccession.objects.filter(accession=CalledAccn)

    if qq.count() > 0:
        FmtData = BuildAccTrack(qq,ContID)
    else:
        FmtData = "[]"

#    FmtData = BuildAccTrack(qq,ContID)

    return HttpResponse(FmtData, mimetype='application/json')

########################################################################################
#
#   SERVICE: PostBack
#
#   URL: .../postback/<P1>/<P2>/<P3>/<P4>/<P5>/<P6>
#
#   P1: Request_ID:
#   P2: Contact_ID:
#   P3: Phone_Stat_ID:
#   P4: Status:
#   P5: User_ID:
#   P6: Phone_ID:
#
#   DESCRIPTION: Provides method for passing request updates to csm_requests table.
#
#   FUNCTION(S) CALLED:
#
########################################################################################

def PostBack(request, Fnc, RequestID_in, ContactID_in, PhoneStatID_in, Status_in, UserID_in, PhoneID_in):
    try:
        qq=CsmRequests.objects.get(csm_req_id=RequestID_in)
    except:
        return HttpResponse("[]")

    PostACK=BuildPostBack( qq, ContactID_in, PhoneStatID_in, Status_in, UserID_in, PhoneID_in)

#    return HttpResponse(PostACK, mimetype='application/json')
    return HttpResponse(PostACK)

########################################################################################
#
#   SERVICE: AccessionQuery
#
#   URL: .../acctrack/<P1>
#
#   P1: Accession Number
#
#   DESCRIPTION: Provides method for passing from HNAM all orders and associated info
#                related to the accession specified as an input parameter to the service.
#
#   FUNCTION(S) CALLED: BuildAccQuery
#
########################################################################################

def AccessionQuery(request, Fnc, Accession_in):

    CalledAccn = parseFmtAccn(Accession_in)
    ContID = getContainerID(Accession_in)

    qq=AccessionOrderR.objects.filter(accession=CalledAccn)

    if qq.count() > 0:
        FmtData = BuildAccOrdLst(qq, ContID)
    else:
        FmtData = "[]"

    return HttpResponse(FmtData, mimetype='application/json')

########################################################################################
#
#   SERVICE: AvailableRequestStatuses
#
#   URL: .../AvailableRequestStatuses/
#
#   P1: <NONE>
#
#   DESCRIPTION: Passes all CSM Request statuses for the front 
#                end application to build a list.
#
#   FUNCTION(S) CALLED: BuildRequestStatuses
#
########################################################################################

def AvailableRequestStatuses(request, Fnc):

    qq=CodeValue.objects.filter(code_set=2213)

    if qq.count() > 0:
        StatData = BuildRequestStatuses ( qq )
    else:
        StatData = "[]"

    return HttpResponse(StatData, mimetype='application/json')


########################################################################################
#
#   SERVICE: AvailableRequestStatuses
#
#   URL: .../
#
#   P1: <NONE>
#
#   DESCRIPTION: Return all available CSM Request statuses for the front end application to build a list.
#
#   FUNCTION(S) CALLED: BuildRequestStatuses
#
########################################################################################

def AvailableRequestStatuses(request, Fnc):

    qq=CodeValue.objects.filter(code_set=2213)

    if qq.count() > 0:
        FmtData = BuildRequestStatuses(qq)
    else:
        FmtData = "[]"

    return HttpResponse(FmtData, mimetype='application/json')


########################################################################################
#
#   SERVICE: OrgContacts
#
#   URL: .../
#
#   P1: Organization ID    
#
#   DESCRIPTION: Provides method for passing an ORGANIZATION_ID to retrieve the details
#                of all contacts for the given organization
#
#   FUNCTION(S) CALLED: BuildOrgContacts
#
#######################################################################################

def OrgContacts(request, Fnc, OrgID_in):

    oo=PrsnlOrgReltn.objects.filter(organization_id=OrgID_in).filter(active_ind=1)

    if oo.count() > 0:
        data = BuildOrgContacts( oo )
    else:
        data = ""

    return HttpResponse(data)

########################################################################################
#
#   SERVICE: OrgPhoneInfo
#
#   URL: .../
#
#   P1: Organization ID
#
#   DESCRIPTION: Provides method for passing an ORGANIZATION_ID to retrieve
#                all phone numbers associated with an organization.
#
#   FUNCTION(S) CALLED: BuildPhoneInfo
#
#######################################################################################

def OrgPhoneInfo(request, Fnc, OrgID_in):

    ph=Phone.objects.filter(parent_entity_id=OrgID_in).filter(parent_entity_name='ORGANIZATION').filter(active_ind=1)

    if ph.count() > 0:
        data = BuildPhoneInfo( ph )
    else:
        data = ""

    return HttpResponse(data)

########################################################################################
#
#   SERVICE: AvailablePhoneStatuses
#
#   URL: .../
#
#   P1: <NONE>
#
#   DESCRIPTION: Return all available phone statuses for the front end application to build a list.
#
#   FUNCTION(S) CALLED: BuildPhoneStatuses
#
#######################################################################################

def AvailablePhoneStatuses(request, Fnc):

    cps=CsmPhoneStats.objects.filter(csm_phone_stat_id__gt=0)

    if cps.count() > 0:
        FmtData = BuildPhoneStatuses(cps)
    else:
        FmtData = "[]"

    return HttpResponse(FmtData, mimetype='application/json')
