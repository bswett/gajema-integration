# testutils.py
#checkData is a generic routine designed to compare values of a known, control set of values to a json object that has been returned from a function
def checkData(self, jsonOutputFromFnc, controlVal):
    #check for multiple objects
    if repr(controlVal)[0] == '[':          #'[' indicates multiple elements of a list, otherwise it will be '{'
        for j in range(0,len(controlVal)):  #for multiple objects, check each one, one at a time
            for key in controlVal[j]:       #iterate through individual fields for the current object
                print 'Checking {0} {1}'.format(key,j)
                self.assertEqual(jsonOutputFromFnc[j][key],controlVal[j][key])
    else:
    #iterate over all fields in the control dictionary and check against fields in the returned JSON
        for key in controlVal:
            print 'Checking {0}'.format(key)
            self.assertEqual(jsonOutputFromFnc[key],controlVal[key])

def testHeaderNoRes (callingTest):
    print '==========================================================='
    print ' {0} '.format(callingTest)

def testHeader (callingTest, expectedRes):
    print '==========================================================='
    print ' {0} -- should return {1}'.format(callingTest,expectedRes)

def testFooter (callTest, retVal):
        print '====>>'
        print retVal
        print '\n=============== End Test - {0} ==============================================\n'.format(callTest)