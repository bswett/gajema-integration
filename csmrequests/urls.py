from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable to admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('csmrequests.views',
      # Examples:
      #url(r'^$', 'api.Views.home', name='home'),
      url(r'^(?P<Fnc>(?i)open)/(?P<OrgID_in>\d+)/$', 'OpenRequests'),
      url(r'^(?P<Fnc>(?i)all)/(?P<StartDT>\d{4}-\d{2}-\d{2})/(?P<EndDT>\d{4}-\d{2}-\d{2})/$', 'AllRequests'),
      url(r'^(?P<Fnc>(?i)all)/(?P<StartDT>\d{4}-\d{2}-\d{2})/(?P<EndDT>\d{4}-\d{2}-\d{2})/(?P<OrgID_in>\d+)/$', 'AllRequestsOrg'),
      url(r'^(?P<Fnc>(?i)viewer)/(?P<RequestID_in>\d+)/$', 'CsmViewer'),
      url(r'^(?P<Fnc>(?i)acctrack)/(?P<Accession_in>[A-Z0-9-]+)/$', 'AccessionTracking'),
      url(r'^(?P<Fnc>(?i)accquery)/(?P<Accession_in>[A-Z0-9-]+)/$', 'AccessionQuery'),
      url(r'^(?P<Fnc>(?i)postback)/(?P<RequestID_in>\d+)/(?P<ContactID_in>\d+)/(?P<PhoneStatID_in>\d+)/(?P<Status_in>[A-Z-]+)/(?P<UserID_in>\d+)/(?P<PhoneID_in>\d+)/$', 'PostBack'),
      url(r'^(?P<Fnc>(?i)AvailableRequestStatuses)/$', 'AvailableRequestStatuses'),
      url(r'^(?P<Fnc>(?i)OrgContacts)/(?P<OrgID_in>\d+)/$', 'OrgContacts'),
      url(r'^(?P<Fnc>(?i)AvailablePhoneStatuses)/$','AvailablePhoneStatuses'),
      url(r'^(?P<Fnc>(?i)OrgPhoneInfo)/(?P<OrgID_in>\d+)/$','OrgPhoneInfo'),
)
