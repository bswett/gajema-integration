from csmrequests.models import *
from django.core import serializers
import json

def main(req_id):
    def getAccnInfo(accnID):
        ca=ContainerAccession.objects.filter(accession_id=accnID)
        ce=ContainerEvent.objects.filter(container_id=ca[0].container_id)
        srchCodes=list(ce.values_list('event_type_cd',flat=True))+list(ce.values_list('current_location_cd',flat=True))
        cv=CodeValue.objects.filter(code_value__in=srchCodes)
        print "***Accession and container info (for orders.json)***"
        print serializers.serialize('json',ca)
        print serializers.serialize('json',ce)
        print "**These go in codevals.json**"
        print serializers.serialize('json',cv)

    def getOrgContacts(orgID):
        print ""
        print "Add to person.json for the org contacts query"
        por = PrsnlOrgReltn.objects.filter(organization_id=966175)[:5]
        print serializers.serialize('json',por)
        print "These should also be added to person.json"
        print serializers.serialize('json',Prsnl.objects.filter(person_id__in=list(por.values_list('person_id',flat=True)))\
)
        return

    csm = CsmRequests.objects.filter(pk=req_id)
    print ""
    print "*****Add to csmreq.json*****"
    print serializers.serialize('json',csm)
    print ""
    print "Org stuff"
    #organization stuff
    print serializers.serialize('json',Organization.objects.filter(pk=csm[0].organization_id))
    #phone info
    print ""
    print "*****Add to phone.json*****"
    print "**Add these (or a subset) for the orgphoneinfo requirement***"
    print serializers.serialize('json',Phone.objects.filter(parent_entity_name='ORGANIZATION').filter(parent_entity_id=csm[0].organization_id))
    print ""
    print "The following phone objects must all be added, but some may be duplicates from above"
    print serializers.serialize('json',Phone.objects.filter(pk=csm[0].phone_id))
    print ""
    print "***Add to csmxref.json****"
    cx=CsmReqRefCdXref.objects.filter(pk=csm[0].csm_req_id)
    print serializers.serialize('json',cx)
    if cx[0].parent_entity_name.strip() == "ORDERS":
        order = Orders.objects.filter(pk=cx[0].parent_entity_id)
        print ""
        print "*****Add to orders.json*****"
        print serializers.serialize('json',order)
        print ""
        aor = AccessionOrderR.objects.filter(pk=order[0].order_id)
        print serializers.serialize('json',aor)
        print ""
        print serializers.serialize('json',Accession.objects.filter(pk=aor[0].accession_id_id))
        print ""
        getAccnInfo(aor[0].accession_id_id)
        print "*****Add to encounter.json*****"
        print serializers.serialize('json',Encounter.objects.filter(pk=order[0].encntr_id))
        print ""
        print "*****Add to person.json*****"
        print serializers.serialize('json',Person.objects.filter(pk=order[0].person_id))
        print ""
        print serializers.serialize('json',PersonAlias.objects.filter(person_id=order[0].person_id))
        print ""
        print serializers.serialize('json',Prsnl.objects.filter(pk__in=[order[0].last_update_provider,csm[0].updt_id]))
    else:
        enctr = Encounter.objects.filter(pk=cx[0].parent_entity_id)
        print ""
        print "*****Add to encoutner.json*****"
        print serializers.serialize('json',enctr)
        print ""
        print "*****Add to person.json*****"
        print serializers.serialize('json',Person.objects.filter(pk=enctr[0].person_id))
        print ""
        print serializers.serialize('json',PersonAlias.objects.filter(person_id=enctr[0].person_id))
        print ""
        print serializers.serialize('json',Prsnl.objects.filter(pk=csm[0].updt_id))
    getOrgContacts(csm[0].organization_id)
                     
