"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from csmrequests.models import *
from csmrequests.views import *
from django.core  import serializers
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import datetime
from collections import namedtuple
import json
from csmrequests.testutils import *

#dictionary to store test data for postback
pbdata = [{'PhoneID' : 3673040, 'PhoneStatID' : 8613205, 'CsmStat' : 'WAIT'},
      {'PhoneID' : 3673040, 'PhoneStatID' : 8613203, 'CsmStat' : 'NEW'},
      {'PhoneID' : 3673040, 'PhoneStatID' : 8613204, 'CsmStat' : 'COMPLETED'}]

invalidPbData = [{'PhoneID' : 3673040, 'PhoneStatID' : 8613205, 'CsmStat' : 'WAIT', 'ContactID':312392, 'UserID':2805346, 'Invalid':'ContactID'},
      {'PhoneID' : 3673040, 'PhoneStatID' : 8613203, 'CsmStat' : 'NEW', 'ContactID':3123924, 'UserID':280534, 'Invalid':'UserID'}, 
      {'PhoneID' : 367304, 'PhoneStatID' : 8613204, 'CsmStat' : 'COMPLETED', 'ContactID':3123924, 'UserID':2805346, 'Invalid':'PhoneID'}, 
      {'PhoneID' : 3673040, 'PhoneStatID' : 9999999, 'CsmStat' : 'COMPLETED', 'ContactID':3123924, 'UserID':2805346, 'Invalid':'PhoneStatID'}, 
      {'PhoneID' : 3673040, 'PhoneStatID' : 8613204, 'CsmStat' : 'YOURMOMDOTCOM', 'ContactID':3123924, 'UserID':2805346, 'Invalid':'CsmStat'}] 
      
    
print '***********************************************************************'
print '*                           MOD#  BY  PIM#  PROG ID                   *'
print '*                           ---- --- ----- --------                   *'
print '*                     *RMOD*R000 STH 00000 tests.py                   *'
print '***********************************************************************\n'

# class SimpleTest(TestCase):
    # fixtures = ['orders.json','csmxref.json','phone.json','person.json','encounter.json','csmcat.json','queuedata.json','codeval.json','csmreq.json']
    # def testAvailReqStatuses(self):
        # testHeader('BuildRequestStatuses','valid JSON')
        # qq=CodeValue.objects.filter(code_set=2213)
        # print len(qq)
        # retval = BuildRequestStatuses(qq)
        # print retval
        # jret = json.loads(retval)
        # compareTo = [{"RequestStatDisp": "Completed", "RequestStatID": 1884}, {"RequestStatDisp": "In-Process", "RequestStatID": 1885}, {"RequestStatDisp": "New", "RequestStatID": 1886}, {"RequestStatDisp": "Wait", "RequestStatID": 1887}]
        # checkData(self, jret, compareTo)
        # testFooter('BuildRequestStatuses',retval)

class CsmPostbackTest(TestCase):
    fixtures = ['orders.json','csmxref.json','phone.json','person.json','encounter.json','csmcat.json','queuedata.json','codeval.json','csmreq.json']

    def testGoodData(self):
        testHeaderNoRes('BuildPostBack - Testing Good data')
#            newCsm = CsmRequests()
#        self.assertTrue(newCsm.pk)
#        newCsm.save()
        def chkPostBack(qq, PhoneID, PhoneStatID, CsmStat):
            reqID = 45200512
            print '\n***************************************************************'
            print "Testing CsmStat: {0} and PhoneID: {1}".format(CsmStat, PhoneID)
            sts = BuildPostBack(qq,3123924,PhoneStatID,CsmStat,2805346,PhoneID)
            #re-retrieve row and check for updates
            newqq = CsmRequests.objects.get(pk=reqID)
            now = datetime.datetime.now()
            
            self.assertEqual(CsmStat, newqq.status_cd_id.cdf_meaning)   #status changed to what was passed in?
            #verify updt_dt_tm is current
            self.assertEqual(newqq.updt_dt_tm.date().isoformat(),now.date().isoformat())
            self.assertEqual(newqq.updt_dt_tm.hour, now.hour)
            self.assertEqual(newqq.updt_dt_tm.minute, now.minute)

            print sts
            print 'Updated status for csm_req_id {0} = {1}'.format(reqID,newqq.status_cd_id.cdf_meaning)
            
            #check to be sure the csm_lst_contact table got updated for the phoneID
            cc = CsmLstContact.objects.get(phone_id = PhoneID)
            self.assertEqual(PhoneStatID, cc.csm_phone_stat_id)
            now = datetime.datetime.now()
            #make sure updt_dt_tm has the current time
            self.assertEqual(cc.updt_dt_tm.date().isoformat(),now.date().isoformat())
            self.assertEqual(cc.updt_dt_tm.hour, now.hour)
            self.assertEqual(cc.updt_dt_tm.minute, now.minute)
            print 'Status for PhoneID {0} = {1}'.format(cc.phone_id,cc.csm_phone_stat.csm_phone_stat_desc)
             
        qq = CsmRequests.objects.filter(organization_id=966175)
        self.assertEqual(len(qq), 2)
        qq = CsmRequests.objects.get(pk=45200512)
        self.assertEqual('IN-PROCESS',qq.status_cd_id.cdf_meaning)
        
        #loop through postback test data dictionary
        for j in pbdata:
            chkPostBack(qq, j['PhoneID'], j['PhoneStatID'], j['CsmStat'])
            
        #if last test was COMPLETED status, then csm_compl_dt_tm should have current time.
        if pbdata[len(pbdata)-1]['CsmStat'] == 'COMPLETED':
            now = datetime.datetime.now()
            self.assertEqual(qq.csm_compl_dt_tm.date().isoformat(),now.date().isoformat())
            self.assertEqual(qq.csm_compl_dt_tm.hour, now.hour)
            #be sure the minute didn't roll to the next minute after updating the row
            if qq.csm_compl_dt_tm.minute != now.minute and qq.csm_compl_dt_tm.minute != now.minute-1:
                self.fail('Minute incorrect for csm_cmpl_dt_tm: {0}\nIt should be {1} or {2}'.format(qq.csm_compl_dt_tm.minute,now.minute-1,now.minute))
        testFooter('BuildPostBack - Good data test','')
    def testInvalidData(self):    
        testHeaderNoRes('BuildPostBack - Testing Invalid data')
        def chkInvPostBack(qq, PhoneID, PhoneStatID, CsmStat, ContactID, UserID, InvalidFld):
            print '\n***************************************************************'
            print "Testing invalid input {0}".format(InvalidFld)
            LstContactExsists = True
            try:
                cc1 = CsmLstContact.objects.get(phone_id = PhoneID)
            except:
                LstContactExists = False
                
            stsJson = BuildPostBack(qq,ContactID,PhoneStatID,CsmStat,UserID,PhoneID)
            newqq = CsmRequests.objects.get(pk=45200512)
            #make sure status did NOT change
            self.assertEqual(qq, newqq)     #make sure csmrequests row did not change
            #make sure value returned has status of 0 and also shows 'NACK'
            self.assertIn('"PostStat": 0',stsJson)
            self.assertIn('"ErrorText": "NACK',stsJson)
            print stsJson
            
            #make sure phone status did NOT get updated
            try:
                cc2 = CsmLstContact.objects.get(phone_id = PhoneID)
                if LstContactExists == False:
                    #if the last contact row did not exist then it should not have been inserted
                    self.fail("If CsmLstContact row did not exist before, it should not exist now")
                else:
                    self.assertEqual(cc1,cc2)   #if CsmLstContact row existed before, it should not have changed.
            except:
                pass
            
        qq = CsmRequests.objects.get(pk=45200512)
        for j in invalidPbData:
            chkInvPostBack(qq, j['PhoneID'], j['PhoneStatID'], j['CsmStat'], j['ContactID'], j['UserID'], j['Invalid'])
        testFooter('BuildPostBack - Invalid data test','')

class CsmOpenTest(TestCase):
    fixtures = ['orders.json','csmxref.json','phone.json','person.json','encounter.json','csmcat.json','queuedata.json','codeval.json','csmreq.json']

    def testOpen(self):
        testHeader('BuildRequests','valid JSON')
        #test valid org ID
        OrgID_in = 966175
        print '****** TESTING OPEN REQ for {0}********'.format(OrgID_in)
        qq=CsmRequests.objects.filter(organization=OrgID_in).exclude(status_cd_id__cdf_meaning='COMPLETED')
        qq.count()
        self.assertEqual(len(qq),2)
        print 'calling BuildRequests'
        retval = BuildRequests(qq)
        for j in qq:
            self.assertEqual(j.organization_id,OrgID_in)
        print retval
        #test invalid org ID
        OrgID_in = 966174
        print '****** TESTING OPEN REQ for invalid Org ID {0}********'.format(OrgID_in)
        qq=CsmRequests.objects.filter(organization=OrgID_in).exclude(status_cd_id__cdf_meaning='COMPLETED')
        self.assertEqual(len(qq),0)
        retval = BuildRequests(qq)
        print retval
        testFooter('BuildRequests','')

class CsmViewerTest(TestCase):
    fixtures = ['orders.json','csmxref.json','phone.json','person.json','encounter.json','csmcat.json','queuedata.json','codeval.json','csmreq.json']

    def testViewer(self):
        testHeader('BuildViewRequest','valid JSON')
        
        # #checkData is a generic routine designed to compare values to a known, control set of values to a json object that has been returned from a function
        # def checkData(jsonOutputFromFnc, controlVal):
            # #iterate over all fields in the control dictionary and check against fields in the returned JSON
            # for key in controlVal:
                # print 'Checking {0}'.format(key)
                # self.assertEqual(jsonOutputFromFnc[key],controlVal[key])
            
    
        RequestID_in = 45200512
        print '\n*******TESTING VIEWER REQ FOR {0}***********\n'.format(RequestID_in)
        qq=CsmRequests.objects.get(csm_req_id=RequestID_in)
        retval = BuildViewRequest(qq)
        print retval
        jret = json.loads(retval)
        compareTo = {"StatusCD": "IN-PROCESS", "QueueData": [{"QueueID": 44480367, "QueueName": "NMSSRqueue                              "}], "OrgName": "GB Organization", "CreateDate": "2011-07-20 09:43:09", "ReqType": "NMSSRTEST                               ", "PatDOB": "1988-02-04", "AccNbr": "000002011201000012", "MRN": "201674", "OrgID": 966175, "PatName": "Koraddi, Medicaltest", "CreateUser": "Haven, Scott", "PhysicianName": "Vankadaru, Kiran", "PatSex": "Male", "DueDateTm": "2011-07-20 09:45:09", "SendingSys": "HNAM", "ProbComment": " "}
        checkData(self, jret, compareTo)
        
        RequestID_in = 46092687
        print '\n*******TESTING VIEWER REQ FOR {0}***********\n'.format(RequestID_in)
        qq=CsmRequests.objects.get(csm_req_id=RequestID_in)
        retval = BuildViewRequest(qq)
        print retval
        jret = json.loads(retval)
        compareTo = {"StatusCD": "NEW", "QueueData": [], "OrgName": "GB Organization", "CreateDate": "2011-09-15 11:11:24", "ReqType": "NMScallin                               ", "PatDOB": "1993-04-03", "AccNbr": "", "MRN": "0", "OrgID": 966175, "PatName": "koraddi, Test", "CreateUser": "Vankadaru, Kiran", "PhysicianName": "", "PatSex": "Male", "DueDateTm": "2011-09-15 19:11:24", "SendingSys": "HNAM", "ProbComment": " "}
        checkData(self, jret, compareTo)

        #test invalid req ID by calling CsmViewer. That is where invalid input is handled and the only place
        #that BuildViewRequest is called
        RequestID_in = 98779999999
        print '\n****Testing VIEWER REQ with invalid ID {0}******\n'.format(RequestID_in)
        qq=CsmViewer('req','fnc',RequestID_in)
        self.assertEqual('[]',qq.content)
        print qq.content
        testFooter('BuildRequests','')
       

class CsmAccTrackTest(TestCase):
    fixtures = ['orders.json','csmxref.json','phone.json','person.json','encounter.json','csmcat.json','queuedata.json','codeval.json','csmreq.json']
    accnList = [{'accn':'11-201-00012', 'Containers': 1, 'Events': 3},
                {'accn':'02-038-00699', 'Containers': 2, 'Events': 2},  #only checks the number of events on 1st container
                {'accn':'02-038-00699A', 'Containers': 1, 'Events': 2},
                {'accn':'02-038-00699B', 'Containers': 1, 'Events': 2}]

    def testAccTrack(self):
        testHeader('ACCESSION TRACKING - BuildAccTrack','valid json')
        def callBuildAcc(accn):
            print '\n********TESTING ACCESSION TRACKING REQ FOR {0}************'.format(accn)
            unfmtAccn = parseFmtAccn(accn)
            ContID = getContainerID(accn)
            print 'ContID: {0}'.format(ContID)
            if ContID > 0:
                qq=ContainerAccession.objects.filter(accession=unfmtAccn).filter(accession_container_nbr=ContID)
            else:
                qq=ContainerAccession.objects.filter(accession=unfmtAccn)

            #self.assertTrue(len(qq)>0)
            if len(qq) > 0:
                return BuildAccTrack(qq,ContID)
            else:
                print "No accession found"
                return '[]'
        
        #loop through test data
        for j in CsmAccTrackTest.accnList:
            retval = callBuildAcc(j['accn'])
            accJson = json.loads(retval)
            self.assertEqual(j['Containers'],len(accJson[0]['ContainerInfo']))
            self.assertEqual(j['Events'],len(accJson[0]['ContainerInfo'][0]['ContainerEvents']))
            print retval

        #valid accession with invalid containter ID    
        self.assertEqual('[]',callBuildAcc('02-038-00699C'))
        #invalid accession
        self.assertEqual('[]',callBuildAcc('88-320-99999'))
        testFooter('ACCESSION TRACKING - BuildAccTrack','Success')
        
class CsmLookupTests(TestCase):
    fixtures = ['orders.json','csmxref.json','phone.json','person.json','encounter.json','csmcat.json','queuedata.json','codeval.json','csmreq.json']

    def testOrgPhone(self):
        testHeader('BuildPhoneInfo','valid json')
        OrgID_in = 966175
        ph=Phone.objects.filter(parent_entity_id=OrgID_in).filter(parent_entity_name='ORGANIZATION').filter(active_ind=1)
        retval = BuildPhoneInfo(ph)
        compareTo = [{"PhoneNum": "0000000002", "PhoneID": 3673040, "PhoneExt": "2"}, {"PhoneNum": "0000000001", "PhoneID": 3673039, "PhoneExt": "1"}]
        jret = json.loads(retval)
        compareTo.sort()
        checkData(self, jret, compareTo)
        testFooter('BuildPhoneInfo', retval)
        
    def testAvailablePhoneSts(self):
        testHeader('BuildPhoneStatuses','valid json')
        cps=CsmPhoneStats.objects.filter(csm_phone_stat_id__gt=0)
        retval = BuildPhoneStatuses(cps)
        jret = json.loads(retval)
        compareTo = [{"PhoneStatID": 8613203, "PhoneStat": "Busy                                    "}, {"PhoneStatID": 8613204, "PhoneStat": "Disconnected                            "}, {"PhoneStatID": 8613205, "PhoneStat": "No Answer                               "}, {"PhoneStatID": 8613206, "PhoneStat": "Refused to take message                 "}, {"PhoneStatID": 8613207, "PhoneStat": "Wrong Number                            "}, {"PhoneStatID": 43368486, "PhoneStat": "Out of Range                            "}, {"PhoneStatID": 48862427, "PhoneStat": "Testing Description                     "}, {"PhoneStatID": 48864375, "PhoneStat": "KV Ph Status                            "}, {"PhoneStatID": 48864456, "PhoneStat": "BU test BIG seq                         "}, {"PhoneStatID": 2170358000, "PhoneStat": "LONG Ph status                          "}]
        checkData(self, jret, compareTo)
        testFooter('BuildPhoneStatuses', retval)
        
    def testOrgContacts(self):
        testHeader('BuildOrgContacts','valid json')
        OrgID_in = 966175
        oc = PrsnlOrgReltn.objects.filter(organization_id=OrgID_in).filter(active_ind=1)
        retval = BuildOrgContacts(oc)
        jret = json.loads(retval)
        compareTo = [{"ContactID": 3054417, "ContactName": "Physician ,Physician"}, {"ContactID": 3123924, "ContactName": "Shropshire ,Justin"}, {"ContactID": 3017327, "ContactName": "Tech ,Dba"}, {"ContactID": 2886998, "ContactName": "Premakumar ,Pradeep"}, {"ContactID": 3531931, "ContactName": "Mr.,Hlavaty"}]
        checkData(self, jret, compareTo)
        testFooter('BuildOrgContacts', retval)
        
    def testAvailableReqStatuses(self):
        testHeader('BuildRequestStatuses','valid JSON')
        qq=CodeValue.objects.filter(code_set=2213)
        retval = BuildRequestStatuses(qq)
        print retval
        jret = json.loads(retval)
        compareTo = [{"RequestStatDisp": "Completed", "RequestStatID": 1884}, {"RequestStatDisp": "In-Process", "RequestStatID": 1885}, {"RequestStatDisp": "New", "RequestStatID": 1886}, {"RequestStatDisp": "Wait", "RequestStatID": 1887}]
        checkData(self, jret, compareTo)
        testFooter('BuildRequestStatuses',retval)
