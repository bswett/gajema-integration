from django.db import models
from django.db import connection
from django.conf import settings
import datetime

# Create your models here.

class CodeValue(models.Model):
    code_value = models.IntegerField(primary_key=True, default=0)
    code_set = models.IntegerField(default=0)
    cdf_meaning = models.CharField(max_length=12, blank=True)
    display = models.CharField(max_length=40, blank=True)
    class Meta:
        db_table = u'code_value'

class Organization(models.Model):
    organization_id = models.IntegerField(primary_key=True, default=0)
    org_name = models.CharField(max_length=100)
    class Meta:
        db_table = u'organization'

class Person(models.Model):
    person_id = models.IntegerField(primary_key=True, default=0)
#    person_id_alias = models.ForeignKey('PersonAlias', related_name='+',db_column='person_id')
    birth_dt_tm = models.DateField(null=True, blank=True)
    sex_cd = models.IntegerField(default = 0)
    name_last_key = models.CharField(max_length=100, blank=True)
    name_first_key = models.CharField(max_length=100, blank=True)
    name_full_formatted = models.CharField(max_length=100, blank=True)
    
    def getPersonAlias(self):
        return PersonAlias.objects.get(pk=self.person_id)
        
    class Meta:
        db_table = u'person'

class PersonAlias(models.Model):
    person_alias_id = models.IntegerField(primary_key=True,unique=True)
    person = models.ForeignKey(Person)
    person_alias_type_cd = models.IntegerField(default=0)
    alias = models.CharField(max_length=200)
    active_ind = models.IntegerField(null=True, blank=True, default=0)
    class Meta:
        db_table = u'person_alias'

class Encounter(models.Model):
    encntr_id = models.IntegerField(primary_key=True, default=0)
    person = models.ForeignKey(Person, related_name='+')
    name_full_formatted = models.CharField(max_length=200, blank=True)
    class Meta:
        db_table = u'encounter'

class Prsnl(models.Model):
    person = models.ForeignKey(Person, primary_key=True)
    name_full_formatted = models.CharField(max_length=100, blank=True)
    active_ind = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'prsnl'


class PrsnlOrgReltn(models.Model):
    prsnl_org_reltn_id = models.IntegerField(primary_key=True, default=0)
    person = models.ForeignKey(Prsnl)
    organization = models.ForeignKey(Organization)
    active_ind = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'prsnl_org_reltn'

class Orders(models.Model):
    order_id = models.IntegerField(primary_key=True, default=0)
#    order = models.ForeignKey('AccessionOrderR', primary_key=True, default=0)
    encntr = models.ForeignKey(Encounter, related_name='+')
    person = models.ForeignKey(Person)
    orig_order_dt_tm = models.DateField(null=True, blank=True)
    order_status_cd = models.IntegerField(default=0)
    order_mnemonic = models.CharField(max_length=100, blank=True)
    last_update_provider = models.ForeignKey(Prsnl)
    class Meta:
        db_table = u'orders'

class ContainerAccession(models.Model):
    accession_id = models.IntegerField(default=0)
    container_id = models.IntegerField(primary_key=True, default=0)
    accession = models.CharField(max_length=20, blank=True)
    accession_container_nbr = models.IntegerField(null=True, default=0, blank=True)
    updt_dt_tm = models.DateField()
    updt_id = models.IntegerField(default=0)
    updt_cnt = models.IntegerField(default=0)
    barcode_accession = models.CharField(max_length=20, blank=True)
    container_alias_cd = models.IntegerField(default=0)
    class Meta:
        db_table = u'container_accession'

class ContEventManager(models.Manager):
    def dictfetchall(self,cursor):
        "Returns all rows from a cursor as a dict"
        desc = cursor.description
        return [
            dict(zip([col[0].lower() for col in desc], row))
            for row in cursor.fetchall()
        ]

    def getContEventsByID(self,contID):
        lCursor = connection.cursor()
        lSql = "SELECT container_id,event_type_cd,current_location_cd,updt_dt_tm,updt_id FROM container_event WHERE container_id = %s"
        lCursor.execute(lSql, [contID])
        ContEvents = ContEventManager.dictfetchall(self,lCursor)
        lCursor.close()
        return ContEvents

class ContainerEvent(models.Model):
#    container_event_id = models.IntegerField(primary_key=True)
    container_id = models.IntegerField(default=0)
#uncomment the event_sequence below when running addCsm.py to create fixtures
    event_sequence = models.IntegerField(primary_key=True, default=0)
    event_type_cd = models.IntegerField(default=0)
    current_location_cd = models.IntegerField(default=0)
    updt_dt_tm = models.DateTimeField()
    updt_id = models.IntegerField(default=0)
    objects = ContEventManager()

    class Meta:
        db_table = u'container_event'

class Accession(models.Model):
    accession_id = models.IntegerField(primary_key=True, default=0)
    accession = models.CharField(max_length=20)
    alpha_prefix = models.CharField(max_length=2, blank=True, null=True)
    site_prefix_cd_id = models.ForeignKey('CodeValue', db_column='site_prefix_cd')
    accession_sequence_nbr = models.IntegerField(null=True, default=0, blank=True)
    accession_day = models.IntegerField(null=True, default=0, blank=True)
    accession_year = models.IntegerField(null=True, default=0, blank=True)
    class Meta:
        db_table = u'accession'

class AccessionOrderR(models.Model):
    accession_id = models.ForeignKey('Accession', db_column='accession_id')
    order = models.ForeignKey(Orders, primary_key=True)
    accession = models.CharField(max_length=20, blank=True)
    class Meta:
        db_table = u'accession_order_r'

class Phone(models.Model):
    phone_id = models.IntegerField(primary_key=True, default=0)
    parent_entity_name = models.CharField(max_length=32)
    parent_entity_id = models.IntegerField(default=0)
    active_ind = models.IntegerField(null=True, blank=True)
    phone_num = models.CharField(max_length=100, blank=True)
    extension = models.CharField(max_length=100, blank=True)
    class Meta:
        db_table = u'phone'

class LongText(models.Model):
    long_text_id = models.IntegerField(primary_key=True, default=0)
    long_text = models.TextField()
    class Meta:
        db_table = u'long_text'

class CsmCategories(models.Model):
    csm_cat_id = models.IntegerField(primary_key=True, default=0)
    csm_cat_desc = models.CharField(max_length=40)
    class Meta:
        db_table = u'csm_categories'

class CsmSubCategories(models.Model):
    csm_sub_cat_id = models.IntegerField(primary_key=True, default=0)
    class Meta:
        db_table = u'csm_sub_categories'

class CsmQueues(models.Model):
    csm_queue_id = models.IntegerField(primary_key=True, default=0)
    csm_queue_desc = models.CharField(max_length=40)
    class Meta:
        db_table = u'csm_queues'

class CsmQCatXref(models.Model):
    csm_queue = models.ForeignKey(CsmQueues)
    csm_cat = models.ForeignKey(CsmCategories)
    csm_sub_cat_id = models.IntegerField(primary_key=True, default=0)
    class Meta:
        db_table = u'csm_q_cat_xref'

class CsmReqRefCdXref(models.Model):
    csm_req_id = models.IntegerField(primary_key=True, default=0)
    parent_entity_name = models.CharField(max_length=32)
    parent_entity_id = models.IntegerField(default=0)
#    parent_entity_id_ord = models.ForeignKey(Orders, related_name='+',db_column='parent_entity_id')
#    parent_entity_id_enc = models.ForeignKey(Encounter, related_name='+',db_column='parent_entity_id')
#    parent_entity_id_acc = models.ForeignKey(AccessionOrderR, related_name='+',db_column='parent_entity_id')

    #flex foreign key based on value of parent_entity_name
    def parentObj(self):
        try:
            if self.parent_entity_name.strip() == 'ORDERS':
                return Orders.objects.get(pk=self.parent_entity_id)
            elif self.parent_entity_name.strip() == 'ENCOUNTER':
                return Encounter.objects.get(pk=self.parent_entity_id)
            else:
                return None
        except:
            return None

    def getAccn(self):
        try:
            if self.parent_entity_name.strip() == 'ORDERS':
                return AccessionOrderR.objects.get(pk=self.parent_entity_id)
            else:
                return None
        except:
            return None

    class Meta:
        db_table = u'csm_req_ref_cd_xref'


class CsmPhoneStats(models.Model):
    csm_phone_stat_id = models.IntegerField(primary_key=True)
    csm_phone_stat_desc = models.CharField(max_length=40)
    updt_dt_tm = models.DateField()
    updt_id = models.IntegerField(default=0)
    updt_task = models.IntegerField(default=0)
    updt_cnt = models.IntegerField(default=0)
    updt_applctx = models.IntegerField(default=0)
    class Meta:
        db_table = u'csm_phone_stats'

class CsmLstContact(models.Model):
    csm_lst_rec_id = models.IntegerField(primary_key=True)
    phone = models.ForeignKey(Phone)
    csm_phone_stat = models.ForeignKey(CsmPhoneStats)
    csm_lst_dt_tm = models.DateTimeField()
    csm_contact_id = models.IntegerField(default=0)
    person = models.ForeignKey(Prsnl)
    updt_dt_tm = models.DateTimeField(auto_now=False,auto_now_add=False)
    updt_id = models.IntegerField(default=0)
    updt_task = models.IntegerField(default=0)
    updt_cnt = models.IntegerField(default=0)
    updt_applctx = models.IntegerField(default=0)
    active_ind = models.IntegerField(default=1,null=True, blank=True)
    contact = models.CharField(max_length=100, blank=True)
    class Meta:
        db_table = u'csm_lst_contact'

    def saveLstContact(self, *args, **kwargs):
        if self.csm_lst_rec_id is None:
            if settings.DATABASES['default']['ENGINE'].find('oracle') > 0:
                lCursor = connection.cursor()
                lSql = "SELECT pathnet_seq.nextval FROM dual"
                lCursor.execute(lSql)
                lRows = lCursor.fetchall()
                self.csm_lst_rec_id = lRows[0][0]
                lCursor.close()
            else:
                try:
                    self.csm_lst_rec_id =  self.__class__.objects.all().order_by("-csm_lst_rec_id")[0].csm_lst_rec_id + 1
                except:
                    self.csm_lst_rec_id = 1
        self.updt_dt_tm = datetime.datetime.now().replace(microsecond=0)
        super(self.__class__, self).save(*args, **kwargs)

class CsmRequests(models.Model):
    csm_req = models.ForeignKey(CsmReqRefCdXref, primary_key=True, default=0)
#    csm_req_id2 = models.ForeignKey(CsmReqRefCdXref, related_name='+', db_column='csm_req_id')
    csm_compl_dt_tm = models.DateTimeField(null=True, blank=True)
    organization = models.ForeignKey(Organization)
    status_cd_id = models.ForeignKey(CodeValue, db_column='status_cd')
    csm_ord_dt_tm = models.DateTimeField(null=True, blank=True)
    csm_req_com = models.ForeignKey(LongText)
    csm_cat = models.ForeignKey(CsmCategories)
    csm_sub_cat = models.ForeignKey(CsmSubCategories)
    updt = models.ForeignKey(Prsnl)
    phone = models.ForeignKey(Phone)
    csm_due_dt_tm = models.DateTimeField(null=True, blank=True)
    updt_dt_tm = models.DateTimeField(auto_now=False, auto_now_add=False)
#    updt_id = models.IntegerField(default=0)
    updt_task = models.IntegerField(default=0)
    updt_cnt = models.IntegerField(default=0)
    updt_applctx = models.IntegerField(default=0)
    problem_req_comment = models.ForeignKey(LongText, related_name='+')
    class Meta:
        db_table = u'csm_requests'
        
    def saveCsm(self, *args, **kwargs):
        self.updt_dt_tm = datetime.datetime.now().replace(microsecond=0)
        super(self.__class__, self).save(*args, **kwargs)


